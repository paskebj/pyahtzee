#!/usr/bin/python3

import random
import time

DICE = 5 # number of dice
MAX_THROWS = 3 # number of throws per try
MAX_TRIES = 100 # number of tries allowed
DELAY = 3 # delay in seconds between attempts
RATIO_DECIMALS = 3 # accuracy of wins/tries ratio

wins = 0
tries = 0
while tries < MAX_TRIES:   
    throws = 0
    dice_kept = 0
    scores = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}
    tries += 1
    count_string = f'Try {tries}'
    dash_string = '-' * len(count_string)
    print(count_string)
    print(dash_string)
    while throws < MAX_THROWS:
        for i in range(dice_kept, DICE):
            score = random.randint(1, 6)
            scores[score] += 1
        # determine key with highest score
        max_key = max(scores, key=scores.get)
        dice_kept = scores[max_key]
        # print result of throw
        scores_string = str(max_key) * dice_kept
        scores_string += ' '
        for key in scores:
            if key != max_key:
                for i in range(scores[key]):
                    scores_string += str(key)
                scores[key] = 0 # do not keep die for next throw    
        print(scores_string)
        throws += 1
    if dice_kept == DICE:
        wins += 1
        print('YAHTZEE!!!')
    if wins == 0:
        ratio = MAX_TRIES
    else:
        ratio = round(tries/wins, RATIO_DECIMALS)    
    print(f'Tries: {tries}, yahtzees: {wins}, ratio: {ratio}')
    print()     
    time.sleep(DELAY)


